# `chardet-rs-bin`

`chardet-rs-bin` is a simple Rust binary to detect the encoding of input
text files.

It's similar to the Python [`chardet` implementation][pypy:chardet], but
uses [`chardetng`][chardetng] as its engine.

[pypy:chardet]: https://pypi.org/project/chardet/
[chardetng]: https://github.com/hsivonen/chardetng

## License

This is released under the [MIT License](LICENSE).
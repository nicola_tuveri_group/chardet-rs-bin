PREFIX ?= /usr/local
BIN_NAME = chardet-rs-bin

$(BIN_NAME): target/release/$(BIN_NAME)
	cp $< $@

target/release/$(BIN_NAME): FORCE
	cargo build --release

target/debug/$(BIN_NAME): FORCE
	cargo build

prefix ?= $(PREFIX)
bindir ?= $(prefix)/bin
mandir ?= $(prefix)/share/man/man1
install: $(BIN_NAME)
	mkdir -p $(DESTDIR)$(bindir)
	install -m 755 $(BIN_NAME) $(DESTDIR)$(bindir)/
clean:
	cargo clean
	rm -f $(BIN_NAME)

FORCE:

.PHONY: clean install FORCE

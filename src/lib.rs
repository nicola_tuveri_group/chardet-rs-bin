use chardetng;
use encoding_rs::Encoding;

pub fn chardet(buffer: &Vec<u8>) -> &'static Encoding {
    let mut decoder = chardetng::EncodingDetector::new();

    decoder.feed(buffer, false);

    // Detect charset of file
    let (e, confident) = decoder.guess_assess(None, true);

    if !confident {
        eprintln!("Warning: EncodingDetector guess might be inaccurate.")
    }

    e
}

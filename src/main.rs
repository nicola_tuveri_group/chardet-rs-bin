use std::fs::OpenOptions;
use std::io::Read;

mod lib;

fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.len() != 2 {
        eprintln!("Usage: {} [FILE|-]", args[0]);
        std::process::exit(1);
    }
    let mut buffer: Vec<u8> = Vec::new();

    match args[1].as_str() {
        "-" => {
            let mut r = std::io::stdin().lock();
            r.read_to_end(&mut buffer).expect("Could not read stream");
        }
        other => {
            // Open text file
            let mut file = OpenOptions::new()
                .read(true)
                .open(other)
                .expect("Could not open file");
            file.read_to_end(&mut buffer)
                .expect("Could not read stream");
        }
    };

    // Detect charset of file
    let result = lib::chardet(&buffer);

    println!("{}", result.name());
}
